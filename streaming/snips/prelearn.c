/*
INTEL CONFIDENTIAL

Copyright © 2020 Intel Corporation.

This software and the related documents are Intel copyrighted
materials, and your use of them is governed by the express
license under which they were provided to you (License). Unless
the License provides otherwise, you may not use, modify, copy,
publish, distribute, disclose or transmit  this software or the
related documents without Intel's prior written permission.

This software and the related documents are provided as is, with
no express or implied warranties, other than those that are
expressly stated in the License.
*/

#include "prelearn.h"

static uint16_t localLearningEnabled = 1;
//static int time;

int e[num_classes] = {0}; // For keeping track of errors
int S[num_classes] = {0};

PostTraceEntry post_values[num_classes] = {0};

int post_core_ids[num_classes] = POST_CORE_IDS;
int post_comp_ids[num_classes] = POST_COMP_IDS;

int class = 0;
//int label_rate = LABEL_RATE;
int C = Y1;

int err = 0;
int thresh = timesteps_per_sample/4;

CoreId core;//nx_nth_coreid(learningCore);
NeuronCore *nc;

int do_prelearn(runState *s)
{
    if (learningEnabled)
    {//(localLearningEnabled != learningEnabled) { 
        return 1;
    }
    else 
    {
        for(int i=0; i<num_classes; i++)
        {   
            core.id = post_core_ids[i];
            nc = NEURON_PTR(core);
                 
            post_values[i].Yepoch0 = C; 
            nc->stdp_post_state[post_comp_ids[i]] = post_values[i];
            S[i] = 0;
        }
        return 0;
    }
}

void run_prelearn(runState *s){
    //printf("Running pre-learn mgmt at Time : %d\n",s->time_step);
    
    // Ensure that neurons that shouldn't be learning are not learning
    for(int i=0; i<num_classes; i++)
    {   
         if(i!=class)
         {
             core.id = post_core_ids[i];
             nc = NEURON_PTR(core);
             post_values[i].Yepoch0 = C; 
             nc->stdp_post_state[post_comp_ids[i]] = post_values[i];
             S[i] = 0;
         }
    }
    
    // this is the learning neuron
    core.id = post_core_ids[class];
    nc = NEURON_PTR(core);
    
    //if(s->time_step%timesteps_per_sample == 0)
    //{
        //printf("Do learning at timestep %d\n", s->time_step);
        err = (timesteps_per_sample/2 - S[class]); // -theta (threshold) // Threshold is timesteps_per_sample/2 which in this case is 100. If neurons is spiking 100 times every 200 timesteps then it is not in error, otherwise an error is occuring.
        // Find a way to track number of updates to show ET learning does fewer updates without sacrificing accuracy
        // Threshold is currently not dynamic, should it be?
        if(err != 0 && (err > thresh || err < -thresh))
        {
            e[class] = ((C+err) >= 0) ? C+err : 0;
            S[class] = 0;
        }
        
        else
        {
           // don't update
           e[class] = C;
        }
        post_values[class].Yepoch0 = e[class];
        nc->stdp_post_state[post_comp_ids[class]] = post_values[class];
    //}
    //else
    //{
    //    core.id = post_core_ids[class];
    //     nc = NEURON_PTR(core);
    //     post_values[class].Yepoch0 = C; 
    //     nc->stdp_post_state[post_comp_ids[class]] = post_values[class];
    //}

}