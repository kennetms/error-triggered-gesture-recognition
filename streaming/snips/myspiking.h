/*
INTEL CONFIDENTIAL

Copyright © 2020 Intel Corporation.

This software and the related documents are Intel copyrighted
materials, and your use of them is governed by the express
license under which they were provided to you (License). Unless
the License provides otherwise, you may not use, modify, copy,
publish, distribute, disclose or transmit  this software or the
related documents without Intel's prior written permission.

This software and the related documents are provided as is, with
no express or implied warranties, other than those that are
expressly stated in the License.
*/

#ifndef MY_SPIKING_H
#define MY_SPIKING_H

#include "learn_header.h"

extern uint16_t learningCore;
extern uint16_t synapseFmtIndex;
extern uint16_t learningEnabled;

extern int class;

int do_spiking(runState *s);
void run_spiking(runState *s);

typedef struct __attribute__((packed)) {
  uint16_t axon;
  uint8_t core, chip;
  uint16_t numSpikes;
  uint16_t interval;
  uint16_t synapseFmtIndex;
  uint16_t spikeTarget;
  uint16_t unusedB;
  uint16_t unusedC;
  } SpikesIn;

#endif