/*
INTEL CONFIDENTIAL

Copyright © 2020 Intel Corporation.

This software and the related documents are Intel copyrighted
materials, and your use of them is governed by the express
license under which they were provided to you (License). Unless
the License provides otherwise, you may not use, modify, copy,
publish, distribute, disclose or transmit  this software or the
related documents without Intel's prior written permission.

This software and the related documents are provided as is, with
no express or implied warranties, other than those that are
expressly stated in the License.
*/

#include "myspiking.h"
#include "nxsdk.h"
#include "input_header.h" 

static int time = 0;
static ChipId chip;
static CoreId core;
static uint16_t axon;
static uint16_t numSpikes = 0;
static uint16_t interval;
static int count = 0;
static int channelID;
static CoreId core_map[128];
static ChipId chip_map[2];
static SpikesIn spikes_in;

uint16_t learningCore = learningCoreInit;
uint16_t synapseFmtIndex = synapseFmtIndexInit;
uint16_t learningEnabled = 0;

//static int count[10];

int do_spiking(runState *s) {
    return 1;
}

void run_spiking(runState *s) {

    time = s->time_step;
    //printf("%li\n", time);
    // initialize
    if (time==1){
        channelID = getChannelID("spikeAddresses");
        if(channelID == -1) {
              printf("Invalid channelID for spikeAddresses\n");
        }
        //prepopulate the core lookup table
        for(int ii=0; ii<128; ii++)
            core_map[ii] = nx_nth_coreid(ii);
        //prepopulate the core lookup table assumes only 32 chips (bc KB only 2 chips available)
        for(int ii=0; ii<2; ii++)
            chip_map[ii] = nx_nth_chipid(ii);
    }
    

    if(numSpikes>0) {
        count++;
        if (count == interval) {
            count=0;
            numSpikes--;
            if(numSpikes==0)
            {
                printf("DONE LEARNING!");   
            }
            //learningEnabled = 1;
            nx_send_remote_event(time, chip, core, axon); // what does this do?               
        }
    } 
    else {
        learningEnabled = 0;
        if (probeChannel(channelID)) {
            readChannel(channelID, &spikes_in, 1);
            chip = chip_map[spikes_in.chip];
            core = core_map[spikes_in.core];
            axon = (1<<14) | spikes_in.axon;
            interval = spikes_in.interval;
            numSpikes = spikes_in.numSpikes;
            
            learningCore = spikes_in.core;
            synapseFmtIndex = spikes_in.synapseFmtIndex;
            learningEnabled = 1;
            
            class = spikes_in.spikeTarget;
            printf("chip %d core %d axon %d interval %d numSpikes %d synapseFmtIndex %d class %d \n", chip, learningCore, axon, interval, numSpikes, synapseFmtIndex, class);
        }
        else {
            learningEnabled = 0;
        }
    }
}
