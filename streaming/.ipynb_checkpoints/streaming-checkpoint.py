# INTEL CONFIDENTIAL
#
# Copyright © 2020 Intel Corporation.
#
# This software and the related documents are Intel copyrighted
# materials, and your use of them is governed by the express
# license under which they were provided to you (License). Unless
# the License provides otherwise, you may not use, modify, copy,
# publish, distribute, disclose or transmit  this software or the
# related documents without Intel's prior written permission.
# 
# This software and the related documents are provided as is, with
# no express or implied warranties, other than those that are
# expressly stated in the License.

import os
import inspect
import errno
import numpy as np
import nxsdk.api.n2a as nx
import scipy.sparse as sps
from nxsdk.graph.nxboard import N2Board

class StreamInputSpikes():
    def __init__(self, net, numNodes, spikesPerPacket, microsecondsPerTimestep):
        self.numNodes = numNodes
        self.net = net
        self.spikesPerPacket = spikesPerPacket
        self.microsecondsPerTimestep = microsecondsPerTimestep
        
        self.snipDir = os.path.abspath(os.path.dirname(inspect.getfile(StreamInputSpikes)) + "/snips")
        self.tempDir = os.path.abspath(os.path.dirname(inspect.getfile(StreamInputSpikes)) + "/temp") 
        try:
            os.mkdir(self.tempDir)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise e

        self._makeInputLayer()
        self._writeHeader()
        
        self.loihiTime = 0
        self.timeReserved = int(1<<14) #special value for timer advance
        self.nothingReserved = int(1<<13) #special value for do nothing
        self.snipdata = np.full((self.spikesPerPacket*2,), self.timeReserved, dtype=int)
        self.snipIndex = 0
        self.packedPerPacket = spikesPerPacket//16
        
        
    def _makeInputLayer(self):
        """Create a new input layer
        """
        
        connProto = nx.ConnectionPrototype(signMode=nx.SYNAPSE_SIGN_MODE.MIXED, 
                                           weight=2,
                                           numDelayBits=0,
                                           numTagBits=0)

        compProto = nx.CompartmentPrototype(vThMant=1,
                                            compartmentVoltageDecay = 4095,
                                            compartmentCurrentDecay = 4095)

        # create the input layer
        self.inputLayer = self.net.createCompartmentGroup(size=self.numNodes, prototype=compProto)


        # create a dummy input connection. This creates the input axons our snips will send spikes to
        inStubGroup = self.net.createInputStubGroup(size = self.numNodes)

        self.inputConnectionGroup = inStubGroup.connect(self.inputLayer,
                                                        prototype = connProto,
                                                        connectionMask = sps.identity(self.numNodes))

    def _writeHeader(self):
        """
        Writes the temporary header file which defines constants used by snips.
        
        :param connectionGroup inputConnectionGroup: Connections to the input neurons
        :param int spikesPerPacket: How many spikes will be communicated in each channel packet
        :param int microsecondsPerTimestep: The desired duration of a loihi timestep in wallclock time (microseconds)
        """
        
        extraHeaderFilePath = self.snipDir + '/input_header.h'
        f = open(extraHeaderFilePath, "w")
        f.write('/* Temporary generated file for defining parameters for input spike streaming*/\n')
        f.write('#define num_addresses ' + str(self.inputConnectionGroup.numNodes)+'\n')
        f.write('#define spikes_per_packet ' + str(self.spikesPerPacket)+'\n')
        f.write('#define us_per_timestep ' + str(self.microsecondsPerTimestep)+'\n')

        f.close()
        
    def setupSpikeInjection(self, board, regenerateCoreAxon=True):
        """Determines the core/axon location of the model input axons and sets up snips
        which will later be used to inject spikes
        
        .. note: spike injection makes the simplifying assumption that all input connection lie \
        on the first loihi chip. Input neurons on later chips are not supported at the moment.
        
        :param ConnectionGroup inputConnectionGroup: The input connection group for the model
        :param N2Board board: The compiled board object
        :param int spikesPerPacket: The number of spikes to send per packet
        :param bool regenerateCoreAxon: Whether to load core/axon values from file or precompute them
        """
        
        # This is incredibly slow. Save the result and load from file
        if regenerateCoreAxon is True:
            # Determine the core/axon addresses
            self.chip = [int]*self.numNodes
            self.core = [int]*self.numNodes
            self.axon = [int]*self.numNodes
            for ii, conn in enumerate(self.inputConnectionGroup):
                (_, self.chip[ii], self.core[ii], self.axon[ii]) = self.net.resourceMap.inputAxon(conn.inputAxon.nodeId)[0]
            np.save(self.tempDir+'/axon', self.axon)
            np.save(self.tempDir+'/core', self.core)
            np.save(self.tempDir+'/chip', self.chip)
        else:
            self.axon = np.load(self.tempDir+'/axon.npy')
            self.core = np.load(self.tempDir+'/core.npy')
            self.chip = np.load(self.tempDir+'/chip.npy')

       
        spikeSnip = board.createProcess(name="inputSpikes",
                                        includeDir=self.snipDir,
                                        cFilePath=self.snipDir + "/myspiking.c",
                                        funcName="run_spiking",
                                        guardName="do_spiking",
                                        phase="spiking",
                                        lmtId=1)

        #self.spikeChannels = board.createChannel(('spikeAddresses').encode(), messageSize=16*4, numElements=self.spikesPerPacket*4)
        self.spikeChannels = board.createChannel(('spikeAddresses').encode(), messageSize=64, numElements=2*self.spikesPerPacket//16)
            
        self.spikeChannels.connect(None, spikeSnip)
    
    def sendSpikes(self, spikeTargets, spikeTimes):
        """
        spikes: a list or numpy array of integers specifying which input neuron should spike
        times: a list or numpy array of monotonically increasing integers corresponding to the time at which the neuron should spike
        """
        
        for ii in range(len(spikeTimes)):
            streamTime = spikeTimes[ii]
            while(self.loihiTime<streamTime):
                self.snipIndex+=2
                self.loihiTime+=1
                if self.snipIndex == self.spikesPerPacket*2:
                    self._transmitSpikes()
            
            self.snipdata[self.snipIndex] = self.axon[spikeTargets[ii]]
            self.snipIndex+=1
            self.snipdata[self.snipIndex] = self.core[spikeTargets[ii]] | np.left_shift(self.chip[spikeTargets[ii]], 8)
            self.snipIndex+=1
            if self.snipIndex == (self.spikesPerPacket*2):
                self._transmitSpikes()
    
    def _transmitSpikes(self):
        
        snipMessage = np.left_shift(self.snipdata[1::2], 16) + np.bitwise_and(self.snipdata[0::2], (2**16)-1)
        #self.spikeChannels.write(self.packedPerPacket, snipMessage)
        #self.spikeChannels.write(self.spikesPerPacket, snipMessage)
        self.spikeChannels.write(self.spikesPerPacket//16, snipMessage)
        
        self.snipdata = np.full((self.spikesPerPacket*2,), self.timeReserved, dtype=int)
        self.snipIndex = 0
        #print(self.loihiTime)
        
    def flushSpikes(self):
        """
        Send any buffered spikes, even if they don't constitute a full packet
        """
    
        while self.snipIndex < (self.spikesPerPacket*2):
            self.snipdata[self.snipIndex] = self.nothingReserved
            self.snipIndex+=1
            self.snipdata[self.snipIndex] = self.nothingReserved
            self.snipIndex+=1
        
        self._transmitSpikes()            
    
    def advanceTime(self, endTime):
        """
        allows time to be advanced on the chip past the last transmitted spike time
        """
         
        while(self.loihiTime<endTime):
            self.snipIndex+=2
            self.loihiTime+=1
            if self.snipIndex == (self.spikesPerPacket*2):
                self._transmitSpikes()
        
        self.flushSpikes()
        
class StreamOutputSpikes():
    def __init__(self, compartmentGroup, interval):
        self.compartmentGroup = compartmentGroup
        self.numClasses = self.compartmentGroup.numNodes
        self.interval = interval
        self._setupSpikeCounters()
        self.snipDir = os.path.abspath(os.path.dirname(inspect.getfile(StreamInputSpikes)) + "/snips")
        self.numOutputs = self.compartmentGroup.numNodes
        # how many packed (16*32 bit) words per packet?
        self.numPacked=1+(self.compartmentGroup.numNodes-1)//16
        
        self._writeHeader()
        
    def _setupSpikeCounters(self):
        """
        Creates dummy probes to setup the spike counters
        
        :param CompartmentGroup outputLayer: The compartments of the last layer
        
        :returns: The dummy probes used to setup spike counters
        :rtype: list-probes
        """
        probeCond = nx.SpikeProbeCondition(tStart=100000000)
        self.probes = self.compartmentGroup.probe(nx.ProbeParameter.SPIKE, probeCond)
        
        
    def _writeHeader(self):
        """
        Writes the temporary header file which defines constants used by snips.
        
        :param connectionGroup inputConnectionGroup: Connections to the input neurons
        :param int spikesPerPacket: How many spikes will be communicated in each channel packet
        :param int microsecondsPerTimestep: The desired duration of a loihi timestep in wallclock time (microseconds)
        """
        
        extraHeaderFilePath = self.snipDir + '/output_header.h'
        f = open(extraHeaderFilePath, "w")
        f.write('/* Temporary generated file for defining parameters for output spike streaming*/\n')
        f.write('#define timesteps_per_sample ' + str(self.interval)+'\n')
        f.write('#define num_outputs ' + str(self.numOutputs)+'\n')
        f.write('#define num_packed ' + str(self.numPacked)+'\n')
        
        f.close()
        
    def setupSpikeReceiving(self, board):
        chipId = 0
        
        # Get the spikes back from loihi
        runMgmtProcess = board.createProcess("runMgmt",
                                             includeDir=self.snipDir,
                                             cFilePath=self.snipDir + "/runmgmt.c",
                                             funcName="run_mgmt",
                                             guardName="do_run_mgmt",
                                             phase="mgmt",
                                             lmtId=0,
                                             chipId=chipId)

        # Create a channel named spikeCntr to get the spikes count information from Lakemont
        self.spikeCntrChannel = board.createChannel(b'nxspkcntr', messageSize=16*4, numElements=100)

        # Connecting spikeCntr from runMgmtProcess to SuperHost which is receiving spike count in the channel
        self.spikeCntrChannel.connect(runMgmtProcess, None)
        
        self.counterIds = [prb.n2Probe.counterId-32 for prb in self.probes[0].probes]
        
        
    def getResults(self):
        results = np.array(self.spikeCntrChannel.read(self.numPacked))
        results = results[:self.numOutputs]  #remove the extras
        results = results[self.counterIds]
        return results