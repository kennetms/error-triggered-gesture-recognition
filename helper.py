import os
import sys
import numpy as np
sys.path.append('/home/kenneth/Documents/nxsdk/0.95/nxsdk-apps/')
import nxsdk.api.n2a as nx
import scipy.sparse as sps
from nxsdk_modules.slayer.src.slayer2loihi import Slayer2Loihi as s2l
from nxsdk_modules.dvs.src.dvs import DVS
import curses
import inspect
import errno
import subprocess
from pynput import keyboard
from streaming.streaming import StreamInputSpikes
import matplotlib.pyplot as plt

def printResult(names, result, stdscr):
    """
    Simple text based printout of results
    """
    count = np.sum(result, axis=0)
    max_count = np.max(count)
    #print(max_count)
    
    if max_count>0:#40:
        count = count#*40//max_count
    
    for (ii, name) in enumerate(names):
        lineString = ' '
        for jj in range(count[ii]):
            lineString = lineString + '-'
        lineString = lineString + '>'
        stdscr.addstr(ii, 0, name + "\t" + lineString + str(count[ii]) + "\t\t\t\t\t\t\t\t")
    
    stdscr.refresh()



def startVisualizer():
    """
    Helper function to launch the DVS visualizer for 128x128 pixels only
    """
    path = '/home/kenneth/Documents/nxsdk/0.95/gesture'

    # compile the visualizer
    subprocess.run(["gcc "
                    "-O3 "
                    + "$(sdl2-config --cflags) "
                    + path + "/visualizer/visualizer_DVS128.c "
                    + "$(sdl2-config --libs) "
                    + "-o "
                    + path + "/visualize_DVS128_spikes"],  shell=True)

    # setup spike fifo
    spikeFifoPath = path + "/spikefifo"

    try:
        os.mkfifo(spikeFifoPath)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise e

    # this environment variables sets where Loihi spikes will appear on the host
    os.environ['NX_SPIKE_OUTPUT'] = spikeFifoPath

    # run the visualizer
    subprocess.Popen(
        [path + "/visualize_DVS128_spikes", "--source=" + spikeFifoPath])


def setupInput(net, corenum):
    """Creates an input compartment group. This must start from core 0 for DVS snip to work"""
    inputSpec = dict()
    inputSpec["sizeX"] = 128
    inputSpec["sizeY"] = 128
    inputSpec["sizeC"] = 2
    compartmentsPerCore = 1024

    layerInput, inputConnectionGroup, corenum = s2l.inputLayer(net, inputSpec, corenum, compartmentsPerCore)

    return layerInput, inputConnectionGroup, corenum

def setupVisualizer(layerInput):
    """Wires up the input to a visualizer using SpikeOutputPorts (Kapoho Bay only)"""
    net = layerInput.net
    opg = net.createSpikeOutputPortGroup(size=layerInput.numNodes)
    layerInput.connect(opg, connectionMask=sps.identity(layerInput.numNodes))


def setupNetwork(layerInput, corenum, modelPath, fileNames, learning=True):
    
    net = layerInput.net


    ######################################################
    # SETUP THE NETWORK AND LEARNING COMPARTMENTS
    ######################################################
    
    # pooling compartments
    compProto = nx.CompartmentPrototype(vThMant = 80, 
                                        compartmentVoltageDecay = 128,
                                        compartmentCurrentDecay = 1024,
                                        refractoryDelay = 1)
    

    print("Creating Learning Compartment Prototype")
    b=14

    plrn = nx.CompartmentPrototype(vThMant=80, # No longer multi-compartment. Neuron corresponding to class should have high activity
                                   biasMant=b,
                                   #biasExp=7,
                                   functionalState=2, # interestingly enough, this compartment that will be the 
                                                      # dendrite of cpB spikes on it's own.
                                   compartmentCurrentDecay=128, # 128
                                   compartmentVoltageDecay=1024, # 1024
                                   enableSpikeBackprop=1,
                                   enableSpikeBackpropFromSelf=1,
                                   refractoryDelay=1)#,
                                   #logicalCoreId=146)

    """
    plrnB = nx.CompartmentPrototype(vThMant=80, # this is the output neuron Emre spoke of.
                                   biasMant=-b,
                                   #biasExp=7,
                                  # functionalState=2,
                                   compartmentCurrentDecay=128,
                                   compartmentVoltageDecay=1024,
                                   #enableSpikeBackprop=1,
                                   #enableSpikeBackpropFromSelf=1,
                                   refractoryDelay=1,
                                   logicalCoreId=146)
    """   

    # Create auxiliary compartment
    # no need to do multi compartment
    """
    cpA = plrnA #dendrite

    cpB = plrnB #soma

    cpB.addDendrite(prototype=cpA,
                    joinOp=nx.COMPARTMENT_JOIN_OPERATION.ADD)

    neuronPrototype = nx.NeuronPrototype(cpB)
    """

    print("Creating Learning Connection Prototype")
    # SGD NEED TO MODIFY FOR ERROR TRIGGERED LEARNING
    dw = '2^-4*x2*(y1-20)*u0-2^-4*x1*(y1-20)*u0'#'-2^-7*x1*y1*u0 + 2^-7*x2*y1*u0 + 1*2^-7*x1*u0 - 1*2^-7*x2*u0'

    lr = net.createLearningRule(dw=dw,
                                x1Impulse=40,
                                x1TimeConstant=3,
                                x2Impulse=40,
                                x2TimeConstant=5,
                                y1Impulse=10,
                                y1TimeConstant=10,
                                tEpoch=20)

    ws=20

    #Data connection prototype
    pconne = nx.ConnectionPrototype(delay=0, 
            enableLearning = learning,
            learningRule = lr, 
            signMode=1, numWeightBits = 8, numDelayBits=0, numTagBits=0, compressionMode=3) #weightExponent=-3 


    ######################################################
    # NETWORK LAYERS SETUP
    ######################################################

    # Make some spike output ports for visualization

    poolSpec = dict()
    poolSpec["stride"] = 4
    poolSpec["compProto"] = compProto
    poolSpec["weightFile"] = modelPath + fileNames[0]#'/pool1.npy'
    compartmentsPerCore = 4096/16

    layer1, corenum = s2l.poolingLayer(layerInput, poolSpec, corenum, compartmentsPerCore)


    convSpec = dict()
    convSpec["compProto"] = compProto
    convSpec["dimX"] = 5
    convSpec["dimY"] = 5
    convSpec["dimC"] = 16
    convSpec["weightFile"] = modelPath + fileNames[1]#'/conv1.npy'
    compartmentsPerCore = 4096/4

    layer2, corenum = s2l.convLayer(layer1, convSpec, corenum, compartmentsPerCore)


    poolSpec = dict()
    poolSpec["stride"] = 2
    poolSpec["compProto"] = compProto
    poolSpec["weightFile"] = modelPath + fileNames[2]#'/pool2.npy'
    compartmentsPerCore = 500#256 

    layer3, corenum = s2l.poolingLayer(layer2, poolSpec, corenum, compartmentsPerCore)

    convSpec = dict()
    convSpec["compProto"] = compProto
    convSpec["dimX"] = 3
    convSpec["dimY"] = 3
    convSpec["dimC"] = 32
    convSpec["weightFile"] = modelPath + fileNames[3]#'/conv2.npy'
    compartmentsPerCore = 130

    layer4, corenum = s2l.convLayer(layer3, convSpec, corenum, compartmentsPerCore)


    poolSpec = dict()
    poolSpec["stride"] = 2
    poolSpec["compProto"] = compProto
    poolSpec["weightFile"] = modelPath + fileNames[4]#'/pool3.npy'
    compartmentsPerCore = 256

    layer5, corenum = s2l.poolingLayer(layer4, poolSpec, corenum, compartmentsPerCore)

    #re-order the compartment group to SlayerPyTorch convention before the fully connected layers
    layer5 = s2l.reorderLayer(layer5)


    fullSpec = dict()
    fullSpec["compProto"] = compProto
    fullSpec["dim"] = 512
    fullSpec["weightFile"] = modelPath + fileNames[5]#'/fc1.npy'
    fullSpec["connProto"] = None
    compartmentsPerCore = 60

    layer6, corenum, conn6  = s2l.fullLayer(layer5, fullSpec, corenum, compartmentsPerCore)

    # not multi-compartment
    fullSpec = dict()
    fullSpec["compProto"] = plrn
    fullSpec["dim"] = 11
    if learning:
        fullSpec["weightFile"] = None
    else:
        fullSpec["weightFile"] = modelPath + fileNames[6]#'/fc2.npy'
    fullSpec["connProto"] = pconne
    compartmentsPerCore = 20
    layerOutput, corenum, connOut = s2l.fullLayer(layer6, fullSpec, corenum, compartmentsPerCore)
    
    labelInput = StreamInputSpikes(net, fullSpec["dim"])

        
    #return output, supervisor, corenum, labelInput
    return layerOutput, corenum, labelInput, connOut

def setupSupervisor(supervisor, corenum):
    net = supervisor.net
    print("Creating SpikeGen Process")
    labelInput = StreamInputSpikes(net, supervisor.numNodes, spikesPerPacket=1024, microsecondsPerTimestep=1)
    labelInput.inputLayer.connect(supervisor, 
        prototype=nx.ConnectionPrototype(), 
        connectionMask=np.identity(supervisor.numNodes))
    
    for cx in labelInput.inputLayer:
        cx.logicalCoreId = int(corenum)
    
    #label_spike_gen = net.createInteractiveSpikeGenProcess(supervisor.numNodes)
    #label_spike_gen.connect(supervisor, prototype=nx.ConnectionPrototype(), connectionMask=np.identity(supervisor.numNodes))
    #return label_spike_gen
    return labelInput

def setupOutput(output):
    dummyProbes = s2l.setupSpikeCounters(output)
    return dummyProbes

# Get the post core and post comp ids of the output neurons from the probe
# This will be used in the learning
def setupIds(sProbe):
    # determine which counterId each compartment is connected to
    counterIds = [prb.n2Probe.counterId for prb in sProbe[0].probes]

    # and which core and compartment it sits on
    postCoreIds = [prb.n2Probe.coreId for prb in sProbe[0].probes]
    postCompIds = [prb.n2Probe.cxId for prb in sProbe[0].probes]

    #make sure all counters are on the same lakemont
    chipId = sProbe[0].probes[0].n2Probe.chipId
    lmtCore = sProbe[0].probes[0].n2Probe.lmtCore
    #assert all(chipId==prb.n2Probe.chipId for prb in sProbe[0].probes), "spike counters are split across multiple chips"
    #assert all(lmtCore==prb.n2Probe.lmtCore for prb in sProbe[0].probes), "spike counters are split across multiple lakemonts on the same chip" 
    """ NEEED TO TURN ABOVE ASSERTIONS BACK ON AFTER BREAKING!"""
    
    return postCoreIds, postCompIds

def changePushMode(board):
    #slow but complete way would be to search for wherever stackOut==1 and 
    # replace it with 3 (assuming we never intend to push V to the stack)
    for ch in range(board.numChips):
        for cc in range(board.n2Chips[ch].numCores):
            for cx in board.n2Chips[ch].n2Cores[cc].cxProfileCfg:
                if cx.stackOut == nx.COMPARTMENT_OUTPUT_MODE.PUSH:
                    cx.stackOut = nx.COMPARTMENT_OUTPUT_MODE.PUSH_U

def visualizeClassification(spikeCntrChannel, numOutputs, counterIds, labelInput, numSteps, sampleLength):
    
    currLabel = "0" # for keeping track of which neurons should be recieving label spikes
    
    #numOutputs = 10 # SHOULD NOT BE HARDCODED ONLY PUTTING IN TO BREAK THINGS!!!!!!!!!!!!
    
    stdscr = curses.initscr()
    curses.noecho()
    curses.cbreak()

    stdscr.nodelay(True)

    numSpikes = 1000 # 1 seconds  #30000# 30 seconds
    spikeTarget = 0
    interval = 1 #send every timestep

    plt.axis()
    f = plt.gcf()
    mgr = plt.get_current_fig_manager()
    mgr.full_screen_toggle()
    plt.pause(0.1)
    width, height = mgr.canvas.get_width_height()
    mgr.full_screen_toggle()
    plt.pause(0.1)
    f.canvas.manager.window.wm_geometry("{}x{}+{}+{}".format(width//2, height, width//2, 0))
    plt.pause(0.1)

    barMaxHeight = 500
    barColor = ['b']*numOutputs
    x1, x2, y1, y2 = plt.axis([0,11,0,barMaxHeight])
    rects = plt.bar(range(1,11), np.zeros((10,)), color=barColor)
    xlabels = ('swipe right   ', 'swipe left', 'right wax\non', 'left wax\non', 'right swipe\ndown', 'left swipe\ndown', 'left\ncome here', 'right\ncome here', 'hello', 'hungry')
    xlabelsNoBreak = ('swipe right', 'swipe left', 'right wax on', 'left wax on', 'right swipe down', 'left swipe down', 'right come here', 'left come here', 'hello', 'hungry')
    plt.xticks(np.arange(1,11), xlabels, rotation=70, fontsize=20, weight="bold")
    plt.yticks([])
    plt.title('-', fontsize=40)
    #plt.tight_layout()
    plt.subplots_adjust(bottom=0.2)

    jj = 0
    numLoops=10
    
    result = np.zeros( (numLoops,numOutputs), dtype=int)

    for ii in range(int(numSteps/sampleLength)):

        # turn on learning after 10 seconds (for ii==50)
        try:
            spikeTarget = int(stdscr.getkey())
            print("Sending Spikes %d\n",spikeTarget)
            labelInput.sendSpikes(spikeTarget, numSpikes, interval)
        #        #print(currLabel)
        except Exception as e:
        #    # no input
            pass
        """if ii == 25:#50:
            print("Sending Spikes %d\n",spikeTarget)
            labelInput.sendSpikes(spikeTarget, numSpikes, interval)
        elif ii == 75:
            print("Sending Spikes again %d\n",spikeTarget)
            labelInput.sendSpikes(spikeTarget, numSpikes, interval)
        elif ii == 125:
            print("Sending Spikes again %d\n",spikeTarget)
            labelInput.sendSpikes(spikeTarget, numSpikes, interval)"""
        

        result[jj] = np.array(s2l.getResults(spikeCntrChannel, 1, numOutputs, counterIds, saveResults=False)[0])
        jj = jj + 1
        if jj == numLoops:
            jj=0
        count = np.sum(result, axis=0)
        max_index = np.argmax(count)
        max_count = count[max_index]

        if max_count>barMaxHeight:
            count = count*barMaxHeight//max_count

        barColor = ['b']*numOutputs
        if max_count > 100:
            barColor[max_index] = 'g'
            plt.title(xlabelsNoBreak[max_index], fontsize=40,weight="bold")
        else:
            plt.title('-', fontsize=40)
        for cc, rect in enumerate(rects):
            rect.set_height(count[cc])
            rect.set_color(barColor[cc])

        plt.pause(0.001)
