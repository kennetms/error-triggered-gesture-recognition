from datetime import datetime
import numpy as np
import torch
from torch.utils.data import Dataset, DataLoader
import slayerSNN as snn
from slayerSNN.quantizeParams import quantizeWeights
from slayerSNN.learningStats import learningStats

class IBMGestureDataset(Dataset):
    def __init__(self, datasetPath, sampleFile, samplingTime, sampleLength):
        self.path = datasetPath 
        self.samplingTime = samplingTime
        self.nTimeBins    = int(sampleLength / samplingTime)
        samples = np.loadtxt(sampleFile).astype('int')
        self.samples = []
        for sample in samples:
            if  sample[1]%2 == 0:
                self.samples.append(sample)

    def __getitem__(self, index):
        # Read inoput and label
        # print(self.samples[index, 0])
        inputIndex  = self.samples[index][0]
        classLabel  = self.samples[index][1] // 2
        # Read input spike
        inputSpikes = snn.io.read2Dspikes(
                        self.path + str(inputIndex.item()) + '.bs2'
                        ).toSpikeTensor(torch.zeros((2,128,128,self.nTimeBins)),
                        samplingTime=self.samplingTime)
        # Create one-hot encoded desired matrix
        desiredClass = torch.zeros((6, 1, 1, 1))
        desiredClass[classLabel,...] = 1

        return inputSpikes, desiredClass, classLabel

    def __len__(self):
        return len(self.samples)

# Define the network
class Network(torch.nn.Module):
    def __init__(self, netParams):
        super(Network, self).__init__()
        # initialize slayer
        slayer = snn.loihi(netParams['neuron'], netParams['simulation'])
        self.slayer = slayer
        # define network functions
        self.conv1 = slayer.conv( 2, 16, 5, padding=2, weightScale=10)
        self.conv2 = slayer.conv(16, 32, 3, padding=1, weightScale=50)
        self.fc1   = slayer.dense((8*8*32), 512)
        self.fc2   = slayer.dense(512, 6)
        self.pool1 = slayer.pool(4)
        self.pool2 = slayer.pool(2)
        self.pool3 = slayer.pool(2)

        self.conv1.weight.data = torch.FloatTensor(np.loadtxt('Trained/conv1Weights.txt').reshape(self.conv1.weight.shape))
        self.conv2.weight.data = torch.FloatTensor(np.loadtxt('Trained/conv2Weights.txt').reshape(self.conv2.weight.shape))
        self.fc1  .weight.data = torch.FloatTensor(np.loadtxt('Trained/fc1Weights.txt').reshape(self.fc1.weight.shape))
        self.fc2  .weight.data = torch.FloatTensor(np.loadtxt('Trained/fc2Weights.txt').reshape(self.fc2.weight.shape))

    def forward(self, spikeInput):
        spike = self.slayer.spikeLoihi(self.pool1(spikeInput )) # 32, 32, 2
        spike = self.slayer.delayShift(spike, 1)
        
        spike = self.slayer.spikeLoihi(self.conv1(spike)) # 32, 32, 16
        spike = self.slayer.delayShift(spike, 1)
        
        spike = self.slayer.spikeLoihi(self.pool2(spike)) # 16, 16, 16
        spike = self.slayer.delayShift(spike, 1)
        
        spike = self.slayer.spikeLoihi(self.conv2(spike)) # 16, 16, 32
        spike = self.slayer.delayShift(spike, 1)
        
        spike = self.slayer.spikeLoihi(self.pool3(spike)) #  8,  8, 32
        spike = spike.reshape((spike.shape[0], -1, 1, 1, spike.shape[-1]))
        spike = self.slayer.delayShift(spike, 1)
        
        spike = self.slayer.spikeLoihi(self.fc1  (spike)) # 512
        spike = self.slayer.delayShift(spike, 1)
        
        spike = self.slayer.spikeLoihi(self.fc2  (spike)) # 11
        spike = self.slayer.delayShift(spike, 1)
        
        return spike

device = torch.device('cuda:3')
netParams = snn.params('network.yaml')
net = Network(netParams).to(device)

error = snn.loss(netParams, snn.loihi).to(device)

testingSet = IBMGestureDataset(datasetPath ='/neuromorphic/shared/Datasets/IBMGesture/DvsGesture_converted_to_bs2_1450/DvsGestureRaw/', 
                               sampleFile  ='/neuromorphic/shared/Datasets/IBMGesture/DvsGesture_converted_to_bs2_1450/test.txt',
                               samplingTime=netParams['simulation']['Ts'],
                               sampleLength=1450)

testLoader  = DataLoader(dataset=testingSet , batch_size=1, shuffle=True, num_workers=1)

stats = learningStats()

for i, (input, target, label) in enumerate(testLoader, 0):
    net.eval()
    with torch.no_grad():
        input  = input.to(device)
        target = target.to(device) 

    output = net.forward(input)

    stats.testing.correctSamples += torch.sum( snn.predict.getClass(output) == label ).data.item()
    stats.testing.numSamples     += len(label)
    # loss = error.numSpikes(output, target)
    # stats.testing.lossSum += loss.cpu().data.item()
    stats.print(0, i)