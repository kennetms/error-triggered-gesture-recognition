import numpy as np

class Event():
    """
    This class provides a way to store, read, write and visualize spike event.

    Members:
        * ``x`` (numpy ``int`` array): `x` index of spike event.
        * ``y`` (numpy ``int`` array): `y` index of spike event (not used if the spatial dimension is 1).
        * ``p`` (numpy ``int`` array): `polarity` or `channel` index of spike event.
        * ``t`` (numpy ``double`` array): `timestamp` of spike event. Time is assumend to be in ms.
    """

    def __init__(self, xEvent, yEvent, pEvent, tEvent):
        if yEvent is None:
            self.dim = 1
        else:
            self.dim = 2

        self.x = xEvent if type(xEvent) is np.array else np.asarray(xEvent) # x spatial dimension
        self.y = yEvent if type(yEvent) is np.array else np.asarray(yEvent) # y spatial dimension
        self.p = pEvent if type(pEvent) is np.array else np.asarray(pEvent) # spike polarity
        self.t = tEvent if type(tEvent) is np.array else np.asarray(tEvent) # time stamp in ms

        self.p -= self.p.min()

        
class IBMGestureDataset():
    def __init__(self, path):
        self.path = path + '/'
        self.labels = np.loadtxt(self.path + 'groundTruth.txt').astype(int)
        
    def __getitem__(self, index):
        filename = self.path + str(index+1) + '.bs2'
        inputSpikes = self.read2Dspikes(filename)
        return inputSpikes, self.labels[index]

    
    def read2Dspikes(self, filename):
        '''
        Reads two dimensional binary spike file and returns a TD event.
        It is the same format used in neuromorphic datasets NMNIST & NCALTECH101.

        The binary file is encoded as follows:
            * Each spike event is represented by a 40 bit number.
            * First 8 bits (bits 39-32) represent the xID of the neuron.
            * Next 8 bits (bits 31-24) represent the yID of the neuron.
            * Bit 23 represents the sign of spike event: 0=>OFF event, 1=>ON event.
            * The last 23 bits (bits 22-0) represent the spike event timestamp in microseconds.

        Arguments:
            * ``filename`` (``string``): path to the binary file.

        Usage:

        >>> TD = spikeFileIO.read2Dspikes(file_path)
        '''
        with open(filename, 'rb') as inputFile:
            inputByteArray = inputFile.read()
        inputAsInt = np.asarray([x for x in inputByteArray])
        xEvent =   inputAsInt[0::5]
        yEvent =   inputAsInt[1::5]
        pEvent =   inputAsInt[2::5] >> 7
        tEvent =( (inputAsInt[2::5] << 16) | (inputAsInt[3::5] << 8) | (inputAsInt[4::5]) ) & 0x7FFFFF
        return Event(xEvent, yEvent, pEvent, tEvent/1000)	# convert spike times to ms
