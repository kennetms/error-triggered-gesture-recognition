import sys
import os
import numpy as np
sys.path.append('/home/kenneth/Documents/nxsdk/0.95/nxsdk-apps/')
import nxsdk.api.n2a as nx
import scipy.sparse as sps
from nxsdk_modules.slayer.src.slayer2loihi import Slayer2Loihi as s2l
from nxsdk_modules.dvs.src.dvs import DVS
import inspect
import errno
import time
import subprocess
from nxsdk.graph.nxinputgen.dvsinputgen import DVSSpikeGenerator

from helper import *

loadState = True
saveState = False

#saveState = not loadState
boardName = 'dvs_gestureXZooLearn' #'dvs_gestureZoo3Learn' is current one I am using for demo ## dvs_gestureZoo is the one that uses default parameters and no learning
# dvs_gestureZoo6Learn is one I am using for figures so far, it does okay but could be better. ALmost same as dvs_gesture3Learn but the x impulse values are 40 instead of 30 and time constants to 5 and 3!
# 'dvs_gestureXZooLearn' is the demo model
# 'dvs_gestureBREAKZooLearn' me trying to test limits of learning

if 'PARTITION' in os.environ:
    del os.environ['PARTITION']
if 'SLURM' in os.environ:
    del os.environ['SLURM']
os.environ['KAPOHOBAY'] = "1"

net = nx.NxNet()
corenum = 0

# instantiate a DVS spike gen. Actual wiring will be taken care of by a snip
dvs = DVS(net=net,
		  dimX=240,
          dimY=180,
          dimP=2)

numSteps = 100000000
sampleLength = 20#200
spikesPerPacket = 2048
C = 20 # for error triggered learning

modelPath = '/home/kenneth/Documents/nxsdk/0.95/nxsdk-apps/nxsdk_modules/slayer/src/slayerLoihiModelZoo' + '/03_IBMGesture/Trained'

fileNames = ['/pool1.npy', '/conv1.npy', '/pool2.npy', 
             '/conv2.npy', '/pool3.npy', '/fc1.npy', '/fc2.npy']

layerInput, inputConnectionGroup, corenum = setupInput(net, corenum)
setupVisualizer(layerInput)
output, corenum, labelInput, connOut = setupNetwork(layerInput, corenum, modelPath, fileNames, learning=True)
dummyProbes = setupOutput(output)


startVisualizer()

#############################################
# Compile or otherwise initialize the board #
#############################################
if loadState is False:
    print("Compiling")
    compiler = nx.N2Compiler()
    board = compiler.compile(net)
    #changePushMode(board) shouldn't need to do anymore, not multicompartment
    counterIds = [prb.n2Probe.counterId-32 for prb in dummyProbes[0].probes]
    
    postCoreIds, postCompIds = setupIds(dummyProbes)
else:
    board, counterIds, learningCores, traceProfile, stdpProfile, postCoreIds, postCompIds = s2l.initBoard(boardName)
    DVSSpikeGenerator(board)
    print(board.options)
    

#############################################
######### Setup Channels and SNIPS ##########
#############################################



# and which core and compartment it sits on
#postCoreIds = [prb.n2Probe.coreId for prb in dummyProbes[0].probes]
#postCompIds = [prb.n2Probe.cxId for prb in dummyProbes[0].probes]

#make sure all counters are on the same lakemont
#chipId = dummyProbes[0].probes[0].n2Probe.chipId
#lmtCore = dummyProbes[0].probes[0].n2Probe.lmtCore
#assert all(chipId==prb.n2Probe.chipId for prb in dummyProbes[0].probes), "spike counters are split across multiple chips"
#assert all(lmtCore==prb.n2Probe.lmtCore for prb in dummyProbes[0].probes), "spike counters are split across multiple lakemonts on the same chip"

path = '/home/kenneth/Documents/nxsdk/0.95/gesture/streaming/snips/learn_header.h'

s2l.writeHeader(inputConnectionGroup, output, 0, sampleLength, postCoreIds, postCompIds, C, path)
numOutputs = output.numNodes

spikeCntrChannel = s2l.prepSpikeCounter(board, 100, int(corenum))

regenerateCoreAxon = saveState#not loadState # already have things in temp dir
print('regenerateCoreAxon')
print(regenerateCoreAxon)
channel, core, axon = labelInput.setupSpikeInjection(board, regenerateCoreAxon=regenerateCoreAxon)

#Custom injection snip here
snip = '/home/kenneth/Documents/nxsdk/0.95/gesture/snips/gesture.c'
funcName = "dvs_snip_injection"
guardName = "do_dvs_snip_injection"

dvs.setupSnips(board,
               snip=snip,
               funcName=funcName, 
               guardName=guardName)

#############################################
########### LOAD/SAVE the Board #############
#############################################

board.start()
if saveState is True:
    print("Saving Board")
    s2l.saveBoard(board, boardName, counterIds, learningCores=[output[0].logicalCoreId], postCoreIds=postCoreIds, postCompIds=postCompIds)

if loadState is True:
    print("Loading Board")
    os.environ["DVS_MODE"] = ""
    s2l.loadBoard(board, boardName, learningCores, traceProfile, stdpProfile)

#############################################
############# Run and visualize #############
#############################################

board.run(numSteps, aSync=True)

visualizeClassification(spikeCntrChannel, numOutputs, counterIds, labelInput, numSteps, sampleLength)

board.finishRun()
board.disconnect()