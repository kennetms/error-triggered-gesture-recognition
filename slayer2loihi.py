# INTEL CONFIDENTIAL
# 
# Copyright © 2020 Intel Corporation.
# 
# This software and the related documents are Intel copyrighted
# materials, and your use of them is governed by the express 
# license under which they were provided to you (License). Unless
# the License provides otherwise, you may not use, modify, copy, 
# publish, distribute, disclose or transmit  this software or the
# related documents without Intel's prior written permission.
# 
# This software and the related documents are provided as is, with
# no express or implied warranties, other than those that are 
# expressly stated in the License.

"""Tools for implementing SLAYER-trained models on Loihi"""

import numpy as np
import nxsdk.api.n2a as nx
from nxsdk.graph.nxboard import N2Board
from nxsdk.graph.monitor.probes import *
import scipy.sparse as sps
import pickle
import os
import inspect
import errno
import yaml
import io
        
class Slayer2Loihi():
    """
    A class of static helper functions to assist with implementing 
    SLAYER trained models on Loihi.
    """
    @staticmethod
    def getModels():
        """
        Pulls the latest SLAYER models from a public repo and 
        places them in a subfolder under the slayer module 
        
        :returns: The path to the SLAYER models
        :rtype: string
        """
        # latest tested SLAYER models commit
        commitID='a8ddb8dbe45b36726c8a26c0c630b1ebd866d7c9' 
        
        s2lPath = os.path.abspath(os.path.dirname(inspect.getfile(Slayer2Loihi)))
        modelPath = s2lPath + "/slayerLoihiModelZoo"
        repo = 'https://github.com/bamsumit/slayerLoihiModelZoo.git'
            
        # clone into parent directory to share between
        if not os.path.exists(modelPath):
            os.system('git -C {} clone {}'.format(s2lPath, repo))
        else:
            os.system('git -C {} pull'.format(modelPath))

        os.system('git -C {} checkout {}'.format(modelPath, commitID))
        
        return modelPath


    @staticmethod
    def compartmentPrototype(yamlFile):
        """
        Generates a compartment prototype for the model using a yaml file
        
        :param string yamlFile: Path to the slayer network.yaml file
        
        :returns: The compartment prototype
        :rtype: CompartmentPrototype
        """
        
        with open(yamlFile, 'r') as stream:
            params = yaml.safe_load(stream)["neuron"]
        
        compProto = nx.CompartmentPrototype(vThMant=params["vThMant"], 
                                            compartmentVoltageDecay=params["vDecay"],
                                            compartmentCurrentDecay=params["iDecay"],
                                            refractoryDelay=params["refDelay"])
        return compProto
        
    
    @staticmethod
    def distributeCompartments(layer, corenum, compartmentsPerCore):
        """Distributes compartments across cores, starting on the next available
        core as determined from corenum
        
        :param CompartmentGroup layer: The group of compartments to distribute
        :param float corenum: The last used logicalCoreId
        :param int compartmentsPerCore: The maximum number of compartments per core
        
        :returns: The last used logicalCoreId
        :rtype: float
        """
        
        corenum = np.ceil(corenum)
        for comp in layer:
            comp.logicalCoreId = int(np.floor(corenum))
            corenum = corenum+1/compartmentsPerCore

        return corenum
    
    @staticmethod
    def reorderLayer(layerIn):
        """
        Converts a compartment group from WHC to CHW order.
        
        :param CompartmentGroup layerIn: The layer to reorder.
        
        :returns: The re-ordered layer
        :rtype: CompartmentGroup
        """
        
        net = layerIn.net

        layerOut = net.createCompartmentGroup()
        layerOut.sizeX = layerIn.sizeX
        layerOut.sizeY = layerIn.sizeY
        layerOut.sizeC = layerIn.sizeC

        for cc in range(layerIn.sizeC):
            for yy in range(layerIn.sizeY):
                for xx in range(layerIn.sizeX):
                    layerOut.addCompartments(layerIn[xx*layerIn.sizeY*layerIn.sizeC + yy*layerIn.sizeC + cc])

        return layerOut

    @classmethod
    def inputLayer(cls, net, inputSpec, corenum, compartmentsPerCore):
        """Create a new input layer

        :param dict inputSpec: Specifies the dimensions of the layer, "sizeX", "sizeY", "sizeC"
        :param float corenum: The last used logicalCoreId
        :param int compartmentsPerCore: The maximum number of compartments per core
        
        :returns: 
            - layerInput (compartmentGroup): The input compartment group
            - inputConnectionGroup (ConnectionGroup): connections to the input compartment group 
            - corenum (float): The last used logicalCoreId
        """
        
        sizeX = inputSpec["sizeX"]
        sizeY = inputSpec["sizeY"]
        sizeC = inputSpec["sizeC"]
        protoInput = inputSpec["sizeC"]
        connProto = nx.ConnectionPrototype(signMode = nx.SYNAPSE_SIGN_MODE.MIXED, 
                                           weight = 10,
                                           numDelayBits = 0,
                                           numTagBits = 0)

        compProto = nx.CompartmentPrototype(vThMant = 1,
                                            compartmentVoltageDecay = 4095,
                                            compartmentCurrentDecay = 4095)

        # create the input layer
        layerInput = net.createCompartmentGroup(size=sizeX*sizeY*sizeC, prototype=compProto)

        # add properties used by later convolution
        layerInput.sizeX = sizeX
        layerInput.sizeY = sizeY
        layerInput.sizeC = sizeC

        corenum = cls.distributeCompartments(layerInput, corenum, compartmentsPerCore)

        # create a dummy input connection. This creates the input axons our snips will send spikes to
        inStubGroup = net.createInputStubGroup(size = layerInput.numNodes)

        inputConnectionGroup = inStubGroup.connect(layerInput,
                                                   prototype = connProto,
                                                   connectionMask = sps.identity(layerInput.numNodes))

        return layerInput, inputConnectionGroup, corenum

    @staticmethod
    def writeHeader(inputConnectionGroup, layerOutput, spikesPerPacket, sampleLength):
        """
        Writes the temporary header files which defines constants used by snips.
        
        :param connectionGroup inputConnectionGroup: Connections to the input neurons
        :param compartmentGroup layerOutput: The output compartment group
        :param int spikesPerPacket: How many spikes will be communicated in each channel packet
        :param int sampleLength: The duration of each sample, used to determine how frequently to report output spike counts
        """
        
        extraHeaderFilePath = snipDir + '/array_sizes.h'
        f = open(extraHeaderFilePath, "w")
        f.write('/* Temporary generated file for define the size of arrays before compilation */\n')
        f.write('#define num_addresses ' + str(inputConnectionGroup.numNodes)+'\n')
        f.write('#define spikes_per_packet ' + str(spikesPerPacket)+'\n')
        f.write('#define timesteps_per_sample ' + str(sampleLength)+'\n')
        f.write('#define num_classes ' + str(layerOutput.numNodes)+'\n')
        

        f.close()
        
    @classmethod
    def prepSpikeInjection(cls, inputConnectionGroup, board, spikesPerPacket, sampleLength, numSnips, regenerateCoreAxon):
        """Determines the core/axon location of the model input axons and sets up snips
        which will later be used to inject spikes
        
        .. note: spike injection makes the simplifying assumption that all input connection lie \
        on the first loihi chip. Input neurons on later chips are not supported at the moment.
        
        :param ConnectionGroup inputConnectionGroup: The input connection group for the model
        :param N2Board board: The compiled board object
        :param int spikesPerPacket: The number of spikes to send per packet
        :param int sampleLength: The number of timesteps per sample
        :param int numSnips: The number of Lakemonts to distribute spikes across
        :param bool regenerateCoreAxon: Whether to load core/axon values from file or precompute them
        """
        

        net = inputConnectionGroup.net

        # This is incredibly slow. Save the result and load from file
        if regenerateCoreAxon is True:
            import time
            tStart = time.time()
            # Determine the core/axon addresses
            #chip = [int]*inputConnectionGroup.numNodes
            core = [int]*inputConnectionGroup.numNodes
            axon = [int]*inputConnectionGroup.numNodes
            for ii, conn in enumerate(inputConnectionGroup):
                (_, tempChip, tempCore, axon[ii]) = net.resourceMap.inputAxon(conn.inputAxon.nodeId)[0]
                #core[ii] = board.n2Chips[tempChip].n2Cores[tempCore].id
                core[ii] = tempCore
            np.save(tempDir+'/axon.txt', axon)
            np.save(tempDir+'/core.txt', core)
            tEnd = time.time()
            
        else:
            axon = np.load(tempDir+'/axon.txt')
            core = np.load(tempDir+'/core.txt')

        # Setup Spike Injection Snips
        spikeSnips = [None]*numSnips
        spikeChannels = [None]*numSnips

        for ii in range(numSnips):
            spikeSnips[ii] = board.createProcess(name="runSpikes"+str(ii),
                                                 includeDir=snipDir,
                                                 cFilePath=snipDir + "/myspiking"+str(ii)+".c",
                                                 funcName="run_spiking"+str(ii),
                                                 guardName="do_spiking"+str(ii),
                                                 phase="spiking",
                                                 lmtId=ii)

            spikeChannels[ii] = board.createChannel(('spikeAddresses'+str(ii)).encode(), messageSize=16*4, numElements=spikesPerPacket*4)
            
            spikeChannels[ii].connect(None, spikeSnips[ii])

        return spikeChannels, core, axon
    
    @staticmethod
    def prepSpikeData(core, axon, spikesPerPacket, layerInput, dataset, numSamples, sampleLength, numSnips):
        """
        Prepares spike data for injection
        
        .. note: This function makes the simplifying assumption that all input layer neurons lie on 


        :param list-int core: A list of the core id for each input neuron
        :param list-int core: A list of the axon id for each input neuron
        :param int spikesPerPacket: The number of spikes to send per packet
        :param compartmentGroup layerInput: The input compartment group
        :param dataset dataset: The dataset handle, which returns a sample when indexed
        :param int numSamples: How many samples to prepare
        :param int sampleLength: The number of timesteps per sample
        :param int numSnips: The number of Lakemonts to use for spike injection
        
        :returns:
            - snipdata (list-nparray): Spike data returned as a list with one numpy integer array per Lakemont
            - numSteps (int): The duration of the run
        """
        sizeXin = layerInput.sizeX
        sizeYin = layerInput.sizeY
        sizeCin = layerInput.sizeC

        
        addresses = []
        timestamps = []
        for ii in range(numSamples):
            # Some code here to load x,y,p,ts
            # convert ts to milliseconds
            #sample, _ = dataset[ii]
            sample = dataset[ii]
            #add some blank time between samples
            ts = sample.t.astype(int) + ii*(sampleLength)
            x = sample.x
            y = sample.y
            p = sample.p
            
            addresses.extend((x*sizeYin*sizeCin + y*sizeCin + p).tolist())
            timestamps.extend((ts).tolist())

        numSteps = np.max(timestamps)+1000

        timestamps = np.array(timestamps)
        addresses = np.array(addresses)

        ts = [None]*numSnips
        add = [None]*numSnips
        snipdata = [None]*numSnips
        for ii in range(numSnips):
            ts[ii] = timestamps[ii::numSnips]
            add[ii] = addresses[ii::numSnips]

            timeReserved = int(3<<13) #special value for timer advance

            # combine timestamps and addresses into a single stream
            #round up to a packet + 1 packet
            numEvents = len(add[ii]) + np.max(ts[ii])
            numEvents = int(spikesPerPacket * np.ceil(1+numEvents/spikesPerPacket))

            snipdata[ii] = np.ones((numEvents*2,), dtype=int)*timeReserved
            evTime = 0
            snipIndex = 0
            for jj, loihiTime in enumerate(ts[ii]):
                while(evTime<loihiTime):
                    snipIndex+=2
                    evTime = evTime+1
                snipdata[ii][snipIndex] = axon[add[ii][jj]]
                snipIndex+=1
                snipdata[ii][snipIndex] = core[add[ii][jj]] 
                snipIndex+=1

            snipdata[ii] = np.left_shift(snipdata[ii][1::2], 16) + np.bitwise_and(snipdata[ii][0::2], (2**16)-1)

        return snipdata, numSteps
    
    @staticmethod
    def sendSpikeData(snipData, spikeChannels, spikesPerPacket):
        """Sends spike data over spikeChannels to the model during runtime. 
        
        :param list(int nparray) snipData: Data to send to snips. One nparray per snip.
        :param list(channel) spikeChannels: A list of channels to send the data over
        :param int spikePerPacket: The number of spikes to send per call to channel.write()
        """
        # send spikes here
        wordsPerPacket = int(spikesPerPacket)
        packedPerPacket = int(wordsPerPacket/16)

        index = 0
        packetNum = 0
        #tStart = time.time()
        while index != len(snipData[0]):
            for ii in range(len(spikeChannels)):
                spikeChannels[ii].write(packedPerPacket, snipData[ii][index:(index+wordsPerPacket)])
            index = index + wordsPerPacket
            packetNum = packetNum + len(spikeChannels)
            
    @staticmethod
    def sendSnipInitialization(initChannels, core, axon):
        """Sends the core/axon addresses of input axons to the snips for use as a LUT

        :param list(Channel) initChannels: The initialization channel for each lakemont
        :param list(int) core: The core address for each address
        :param list(int) axon: The axons address for each address
        """
        # send snip initialization data
        for ii in range(len(core)):
            for channel in initChannels:
                #chip is ignored. For now we assume all input compartments lie on the same chip as the snips
                channel.write(3, [0, core[ii], axon[ii]])   

    @classmethod
    def poolingLayer(cls, layerInput, poolSpec, corenum, compartmentsPerCore):
        """Create a new pooling layer. Assumes that pooling and stride are equal 
        (i.e. non-overlapping pooling regions)

        :param CompartmentGroup layerInput: The input to the pooling layer
        :param dict poolSpec: Specifies the "stride", "connProto", "compProto"
        :param float corenum: The last output of distributeCompartments()
        :param int compartmentsPerCore: The maximum number of compartments per core
        
        :returns:
            - layerOutput (CompartmentGroup) : The compartments of the pooling layer
            - corenum (float): The last used logicalCoreId
        """
        # properties of the input layer
        sizeXin = layerInput.sizeX
        sizeYin = layerInput.sizeY
        sizeCin = layerInput.sizeC
        nInput = sizeCin * sizeYin * sizeXin 
        net = layerInput.net

        # properties of the pooling function
        stride = poolSpec["stride"]
        compProto = poolSpec["compProto"]
        weightFile = poolSpec["weightFile"]
        
        W = np.load(weightFile)
        
        if "delayFile" in poolSpec:
            delayFile = poolSpec["delayFile"]
        else:
            delayFile = None
        
        if delayFile is not None:
            delay = np.load(delayFile)
        else: 
            delay = np.zeros((sizeCin,))

        # properties of the output layer
        sizeXout = int(np.ceil(sizeXin/stride))
        sizeYout = int(np.ceil(sizeYin/stride))
        sizeCout = sizeCin
        nOutput = sizeXout * sizeYout * sizeCout
        
        # indices into the input layer
        x = np.arange(sizeXin, dtype=int)
        y = np.arange(sizeYin, dtype=int)
        c = np.arange(sizeCin, dtype=int)

        xx, yy, cc = np.meshgrid(x, y, c)

        # calculate the source addresses as a linear index
        src = (xx*sizeCin*sizeYin + yy*sizeCin + cc).flat

        # calculate the destination addresses as a linear index
        dst = (np.floor(xx/stride)*sizeCout*sizeYout + np.floor(yy/stride)*sizeCout + cc).astype(int).flat
        
        weight = sps.coo_matrix((W[xx%stride, yy%stride].flat,
                                 (dst,src)), 
                                shape=(nOutput,nInput))
        
        connMask = sps.coo_matrix((np.ones(nInput,),
                                   (dst,src)), 
                                  shape=(nOutput,nInput))
        
        maxD = np.max(delay)
        if maxD != 0:
            numDelayBits = np.ceil(np.log2(maxD))
            enableDelay = 1
        else:
            numDelayBits = 0
            enableDelay = 0
        
        connProto = nx.ConnectionPrototype(signMode=nx.SYNAPSE_SIGN_MODE.MIXED, 
                                           numDelayBits=numDelayBits,
                                           enableDelay=enableDelay,
                                           numTagBits=0)

        
        delay = sps.coo_matrix((delay[cc.flat],
                                   (dst,src)), 
                                  shape=(nOutput,nInput))

        for ii in [64, 32, 16, 8]:
            if ii>maxD+1:
                compProto.numDendriticAccumulators = ii
        
        layerOutput = net.createCompartmentGroup(size=nOutput, prototype=compProto)
        layerOutput.sizeX = sizeXout
        layerOutput.sizeY = sizeYout
        layerOutput.sizeC = sizeCout

        layerInput.connect(layerOutput, 
                           prototype=connProto, 
                           connectionMask=connMask, 
                           weight=weight,
                           delay=delay)
        
        corenum = cls.distributeCompartments(layerOutput, corenum, compartmentsPerCore)

        return layerOutput, corenum

    @classmethod
    def convLayer(cls, layerInput, convSpec, corenum, compartmentsPerCore):
        """Create a new convolution layer. Assumes zero padding for 'same' convolution.
        Does not yet support stride.

        :param CompartmentGroup layerInput: The input to the convolution layer
        :param dict convSpec: Specifies "dimX","dimY","dimC" of the filter, 
                             "connProto", "compProto" prototypes of the layer,
                             "weightFile" where the weights can be read in from.
        :param float corenum: The last output of distributeCompartments()
        :param int compartmentsPerCore: The maximum number of compartments per core
        
        :returns:
            - layerOutput (CompartmentGroup): The compartments of the convolution layer
            - corenum (float): The last used logicalCoreId
        """
        # properties of the input layer
        sizeXin = layerInput.sizeX
        sizeYin = layerInput.sizeY
        sizeCin = layerInput.sizeC
        nInput = sizeXin* sizeYin * sizeCin
        net = layerInput.net

        # properties of the convolution function
        #stride = convSpec["stride"] #not implemented yet
        compProto = convSpec["compProto"]
        convX = convSpec["dimX"]
        convY = convSpec["dimY"]
        convC = convSpec["dimC"]
        weightFile = convSpec["weightFile"]
        
        if "delayFile" in convSpec:
            delayFile = convSpec["delayFile"]
        else:
            delayFile = None
                
        W = np.load(weightFile)
        
        if delayFile is not None:
            D = np.load(delayFile)
        else: 
            D = np.zeros((sizeCin,))
        
        maxD = np.max(D)
        if maxD != 0:
            numDelayBits = np.ceil(np.log2(maxD))
            enableDelay = 1
        else:
            numDelayBits = 0
            enableDelay = 0
        
        connProto = nx.ConnectionPrototype(signMode=nx.SYNAPSE_SIGN_MODE.MIXED, 
                                           numDelayBits=numDelayBits,
                                           enableDelay=enableDelay,
                                           numTagBits=0)
        
        # properties of the output layer
        sizeXout = sizeXin # - np.floor(convX/2)
        sizeYout = sizeYin # - np.floor(convY/2)
        sizeCout = convC
        nOutput = sizeCout * sizeYout * sizeXout
        
        for ii in [64, 32, 16, 8]:
            if ii>maxD+1:
                compProto.numDendriticAccumulators = ii
        
        layerOutput = net.createCompartmentGroup(size=nOutput, prototype=compProto)
        layerOutput.sizeX = sizeXout
        layerOutput.sizeY = sizeYout
        layerOutput.sizeC = sizeCout

        ## If not using zero padding
        xDst = np.arange(sizeXout)
        yDst = np.arange(sizeYout)

        xxDst, yyDst = np.meshgrid(xDst, yDst)

        # destination compartment address
        for dx in range(convX):
            xSrc = (xxDst + dx - np.floor(convX/2))
            for dy in range(convY):
                ySrc = (yyDst + dy - np.floor(convY/2))
                valid = np.logical_and(np.logical_and(np.less(xSrc,sizeXin), 
                                                      np.greater_equal(xSrc,0)),
                                       np.logical_and(np.less(ySrc,sizeYin), 
                                                      np.greater_equal(ySrc,0)))
                for cDst in np.arange(sizeCout):
                    #dst = (cDst*sizeYout*sizeXout + yyDst*sizeXout + xxDst)
                    dst = xxDst*sizeYout*sizeCout + yyDst*sizeCout + cDst 
                    dst = dst[valid].flat
                    for cSrc in np.arange(sizeCin):
                        weight = W[cDst, cSrc, dy, dx]
                        delay = D[cSrc] # may require flipping
                        #until we're smarter about weight sharing, let's just omit
                        #any zero valued weights and see how much we can compact the model
                        if weight != 0:
                            connProto.weight = weight
                            connProto.delay = delay
                            src = xSrc*sizeYin*sizeCin + ySrc*sizeCin + cSrc 
                            src = src[valid].flat
                            connMask = sps.coo_matrix((np.ones(len(src),),
                                                       (dst,src)), 
                                                      shape=(nOutput,nInput))
                            layerInput.connect(layerOutput,
                                               prototype=connProto,
                                               connectionMask=connMask)

        corenum = cls.distributeCompartments(layerOutput, corenum, compartmentsPerCore)
        connProto.delay = 0
        connProto.numDelayBits = 0
        connProto.enableDelay = 0
        return layerOutput, corenum

    @classmethod
    def fullLayer(cls, layerInput, fullSpec, corenum, compartmentsPerCore):
        """Create a new fully connected layer.

        :param CompartmentGroup layerInput: The input to the fully connected layer
        :param dict fullSpec: Specifies "dim", the number of neurons,
                             "connProto", "compProto" prototypes of the layer,
                             "weightFile" where the weights can be read in from.
        :param float corenum: The last output of distributeCompartments()
        :param int compartmentsPerCore: The maximum number of compartments per core
        
        :returns:
            - layerOutput (CompartmentGroup): The compartments of the fully connected layer
            - corenum (float): The last used logicalCoreId
        """
        # properties of the input layer
        nInput = layerInput.numNodes
        net = layerInput.net

        # properties of the convolution function
        compProto = fullSpec["compProto"]
        dim = fullSpec["dim"]
        weightFile = fullSpec["weightFile"]
        if "delayFile" in fullSpec:
            delayFile = fullSpec["delayFile"]
        else:
            delayFile = None
        
        if delayFile is not None:
            D = np.load(delayFile)
        else: 
            D = np.zeros((nInput,))
        
        maxD = np.max(D)
        if maxD != 0:
            numDelayBits = np.ceil(np.log2(maxD))
            enableDelay = 1
        else:
            numDelayBits = 0
            enableDelay = 0
        
        
        connProto = nx.ConnectionPrototype(signMode=nx.SYNAPSE_SIGN_MODE.MIXED, 
                                           numDelayBits=numDelayBits,
                                           enableDelay=enableDelay,
                                           numTagBits=0,
                                           compressionMode=3)
        nOutput = dim

        weight = np.load(weightFile)
        delay = np.zeros((nOutput, nInput))
        for ii in range(nInput):
            delay[:,ii] = D[ii]

        for ii in [64, 32, 16, 8]:
            if ii>maxD+1:
                compProto.numDendriticAccumulators = ii
                
        layerOutput = net.createCompartmentGroup(size=nOutput, prototype=compProto)

        layerInput.connect(layerOutput,
                           prototype=connProto,
                           weight=weight,
                           delay=delay)

        corenum = cls.distributeCompartments(layerOutput, corenum, compartmentsPerCore)

        connProto.delay = 0
        connProto.numDelayBits = 0
        connProto.enableDelay = 0
        return layerOutput, corenum
    
    
    
    @classmethod
    def fullLayerMC(cls, layerInput, fullSpec, corenum, compartmentsPerCore, targetInput=None, tg=None):
        """Create a new fully connected layer.

        :param CompartmentGroup layerInput: The input to the fully connected layer
        :param dict fullSpec: Specifies "dim", the number of neurons,
                             "connProto", "compProto" prototypes of the layer,
                             "weightFile" where the weights can be read in from.
        :param int corenum: The last output of distributeCompartments()
        :param int compartmentsPerCore: The maximum number of compartments per core
        """
        # properties of the input layer
        nInput = layerInput.numNodes
        net = layerInput.net

        # properties of the convolution function
        compProto = fullSpec["compProto"]
        connProtoDendrite = fullSpec["connProtoDendrite"]
        connProtoSoma = fullSpec["connProtoSoma"]
        dim = fullSpec["dim"]
        filename = fullSpec["weightFile"]
        nOutput = dim

        # temporary until we have files for weights of all layers
        if filename is not None:
            #weight = np.load(filename) # for pickled files
            weight = np.loadtxt(filename).reshape(nOutput, nInput) # for unpickled
        else:
            np.random.seed(0)
            # Let's try randomly initialized weights instead of zero
            weight = np.ones((nOutput, nInput))*20#np.random.randint(40, size=(nOutput, nInput))#np.ones((nOutput, nInput))*127
            #np.zeros((nOutput, nInput), dtype=int)
            
        print(weight.shape)
        print(weight)

        layerOutput = net.createNeuronGroup(size=nOutput, prototype=compProto)

        conn = layerInput.connect(layerOutput.dendrites[0], prototype=connProtoDendrite, weight=weight)
        #layerInput.connect(layerOutput.dendrites[0], prototype=connProtoDendrite, weight=weight)
        """
        supervisor = net.createInputStubGroup(size=nOutput)
        supervisor.connect(layerOutput.dendrites[0], prototype=connProtoSoma, weight=-20*np.eye(nOutput, dtype='int'))
        supervisor.connect(layerOutput.soma, prototype=connProtoSoma, weight=20*np.eye(nOutput, dtype='int'))
        """
        if targetInput is not None:
            targetInput.connect(layerOutput.dendrites[0], prototype=connProtoSoma, weight=-20*np.eye(nOutput, dtype='int')) # was negative!!!!!!!!!!!!!
            targetInput.connect(layerOutput.soma, prototype=connProtoSoma, weight=20*np.eye(nOutput, dtype='int')) # was 128!
        
        corenum = cls.distributeCompartments(layerOutput, corenum, compartmentsPerCore)

        #return layerOutput, corenum, conn, supervisor
        return layerOutput, corenum, conn



    @staticmethod
    def saveBoard(board, filename, counterIds, learningCores=None):
        """
        Writes an N2Board to a file which can be reloaded later.
        
        :param N2Board board: The board to be written
        :param string filename: The name of the file to write (without extension)
        :param list-probes dummyProbes: The dummy probes used to setup spike counters
        """
        
        # Determine the information required to later load the board
        boardId = board.id
        numChips = len(board.n2Chips)
        numCoresPerChip = [None]*numChips
        numSynapsesPerCore = [None]*numChips
        for ii in range(numChips):
            numCoresPerChip[ii] = len(board.n2Chips[ii].n2Cores)
            numSynapsesPerCore[ii] = [None]*numCoresPerChip[ii]
            for jj in range(numCoresPerChip[ii]):
                numSynapsesPerCore[ii][jj] = board.n2Chips[ii].n2Cores[jj].synapses.numNodes

        traceProfile = None
        stdpProfile = None

        if learningCores is not None:
            traceProfile = dict()
            stdpProfile = dict()
            for lc in learningCores:
                chip = lc//128
                core = lc%128
                name = '{},{}'.format(chip,core)
                traceProfile[name] = []
                stdpProfile[name] = []
                #print(board.n2Chips[chip].n2Cores[core].stdpPostState.data)
                for word in board.n2Chips[chip].n2Cores[core].stdpPostState.data:
                    #print(word)
                    #print(word._traceProfile)
                    #print(word._stdpProfile)
                    traceProfile[name].append(word._traceProfile)
                    stdpProfile[name].append(word._stdpProfile)

        with open(tempDir+ '/'+filename + '.pkl', 'wb') as fname:
            pickle.dump([learningCores, traceProfile, stdpProfile, board.options, board.lmtOptions, counterIds, boardId, numChips, numCoresPerChip, numSynapsesPerCore], fname)

            
        # Dump the NeuroCores
        board.dumpNeuroCores(tempDir+ '/'+filename + '.board')
    
    @staticmethod
    def initBoard(filename):
        """
        Initializes a board object from file with the correct number of chips/cores/synapses.
        
        :param string filename: The name of the file to load from (without extension)
        
        :return N2Board board: The board object
        :returns:
            - board (N2Board): The N2Board object
            - dummyProbes (list-probes): A list of probes used to setup spike counters
        """
        with open(tempDir+ '/'+ filename + '.pkl', 'rb') as fname:
            learningCores, traceProfile, stdpProfile, options, lmtOptions, counterIds, boardId, numChips, numCoresPerChip, numSynapsesPerCore = pickle.load(fname)
            
        board = N2Board(boardId, numChips, numCoresPerChip, numSynapsesPerCore, options=options, lmtOptions=lmtOptions)
        
        return board, counterIds, learningCores, traceProfile, stdpProfile
        
    @staticmethod
    def loadBoard(board, filename, learningCores=None, traceProfile=None, stdpProfile=None):
        """
        Loads neurocore state from file.
        
        :param N2Board board: The board object for which neurocore state should be loaded
        :param string filename: The name of the file to load from (without extension)
        """
        board.loadNeuroCores(tempDir+ '/'+filename + '.board')
        board.sync = False

        if learningCores is not None:
            for lc in learningCores:
                chip = lc//128
                core = lc%128
                name = '{},{}'.format(chip,core)
                for ii in range(len(traceProfile[name])):
                    # automatically extends stdpPostState if ii is out of range
                    board.n2Chips[chip].n2Cores[core].stdpPostState[ii]
                    # than assign the previous values
                    board.n2Chips[chip].n2Cores[core].stdpPostState.data[ii]._traceProfile = traceProfile[name][ii] 
                    board.n2Chips[chip].n2Cores[core].stdpPostState.data[ii]._stdpProfile = stdpProfile[name][ii]
                    
                    board.n2Chips[chip].n2Cores[core].stdpPostState.push(ii)

    @staticmethod
    def setupSpikeCounters(outputLayer):
        """
        Creates dummy probes to setup the spike counters
        
        :param CompartmentGroup outputLayer: The compartments of the last layer
        
        :returns: The dummy probes used to setup spike counters
        :rtype: list-probes
        """
        probeCond = SpikeProbeCondition(tStart=100000000)
        dummyProbes = outputLayer.probe(nx.ProbeParameter.SPIKE, probeCond)
        return dummyProbes
    
    @staticmethod
    def prepSpikeCounter(board, numSamples, corenum):
        """
        Sets up the spike counter snip.
        
        :param N2Board board: The N2 Board object
        :param int numSamples: The number of samples to be processed in this run
        :param float corenum: The logical coreId of the output layer
        
        :returns: The channel over which results will be received
        :rtype: channel
        """
        # Infer which chip the output neurons lie on from the corenum
        chipId = int(corenum/128)
        
        # Get the spikes back from loihi
        runMgmtProcess = board.createProcess("runMgmt",
                                             includeDir=snipDir,
                                             cFilePath = snipDir + "/runmgmt.c",
                                             funcName = "run_mgmt",
                                             guardName = "do_run_mgmt",
                                             phase = "mgmt",
                                             lmtId=0,
                                             chipId=chipId)

        # Create a channel named spikeCntr to get the spikes count information from Lakemont
        spikeCntrChannel = board.createChannel(b'nxspkcntr', messageSize=16*4, numElements=numSamples+2)

        # Connecting spikeCntr from runMgmtProcess to SuperHost which is receiving spike count in the channel
        spikeCntrChannel.connect(runMgmtProcess, None)
        
        return spikeCntrChannel

    @staticmethod
    def getResults(spikeCntrChannel, numSamples, numClasses, counterIds, saveResults=True):
        """
        Reads results from the spike counter channel.
        
        :param channel spikeCntrChannel: The channel over which results will be received
        :param int numSamples: The number of results to receive
        :param int numClasses: The number of classes in each result
        :param list-probes dummyProbes: The dummy probes used to setup spike counters
        :param bool saveResult: Option to save results to file
        
        :returns: The spike count results for the run
        :rtype: nparray-int
        """
        results = spikeCntrChannel.read(numSamples)
        results = np.array(results).reshape((numSamples,16)) #16 due to the "packed" data type
        results = results[:,:numClasses]  #remove the extras
        
        results = results[:, counterIds]

        if saveResults is True:
            np.savetxt(tempDir+'/spikesCounterOut.txt', results, fmt='%i')
        return results
    
    @staticmethod
    def checkAccuracy(labels, results):
        """
        Compares results to dataset labels to calculate an accuracy. 
        In the case of a split decision, an accuracy of 1/winners is assigned
        for the sample.
        
        :param list-int labels: The dataset labels
        :param nparray-int results: The results obtained from getResults()
        
        :returns: The final classification accuracy
        :rtype: float 
        """
        numSamples = results.shape[0]
        classification = [None]*numSamples
        numCorrect = 0
        for ii in range(numSamples):
            maxLogicals = np.amax(results[ii,:])==results[ii,:]
            classification[ii] = np.where(maxLogicals)[0]

            if labels[ii] in classification[ii]:
                numCorrect += 1/len(classification[ii])
        accuracy = numCorrect/len(labels)
        return accuracy

    
snipDir = os.path.abspath(os.path.dirname(inspect.getfile(Slayer2Loihi)) + "/snips")
tempDir = "temp"
try:
    os.mkdir(tempDir)
except OSError as e:
    if e.errno != errno.EEXIST:
        raise e
